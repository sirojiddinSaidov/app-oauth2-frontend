import {useLocation, useNavigate} from "react-router-dom";
import {useEffect} from "react";

const SuccessPage = () => {
    const location = useLocation();
    const navigate = useNavigate();

    useEffect(() => {
        let token = location.search.substring("?token=".length);
        localStorage.setItem("Token", "Bearer " + token);
        navigate("/cabinet")
    }, [])
    return <div>Success page guys</div>
}

export default SuccessPage;