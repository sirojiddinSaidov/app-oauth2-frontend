import React from "react";
import {useLocation, Navigate, Link} from "react-router-dom";
import SignComponent from "../components/Login";
import './index.css'
import SocialLogin from "../components/SocialLogin";


const SignPage = () => {
    let location = useLocation();
    if (localStorage.getItem("AccessToken")) {
        return <Navigate
            to="/cabinet/product"
            state={{from: location}}
            replace/>;
    } else {
        return (
            <>
                <div className="login-container">
                    <div className="login-content">
                        <h1 className="login-title">Login to SpringSocial</h1>
                        <SocialLogin />
                        <div className="or-separator">
                            <span className="or-text">OR</span>
                        </div>
                        <SignComponent/>
                    </div>
                </div>
            </>
        );
    }
}
export default SignPage;