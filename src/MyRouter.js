import {BrowserRouter as Router, Route, Routes} from "react-router-dom";
import Sign from "./pages/sign";
import Home from "./pages/home";
import SuccessPage from "./pages/successPage";
import CabinetPage from "./pages/cabinetPage";
// import Oauth2Handler from "./pages/oauth2Handler";

const MyRouter = () => {
    return (
        <Router>
            <Routes>

                <Route
                    path={"/sign"}
                    element={<Sign/>}
                />
                {/*<Route*/}
                {/*    path={"/oauth2/redirect"}*/}
                {/*    element={<Oauth2Handler/>}*/}
                {/*/>*/}
                <Route
                    path={"/ketmon"}
                    element={<SuccessPage/>}
                />
                <Route
                    path={"/cabinet"}
                    element={<CabinetPage/>}
                />

                <Route
                    path={"/"}
                    element={<Home/>}
                />
            </Routes>
        </Router>
    )
}

export default MyRouter;
