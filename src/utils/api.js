import {BASE_PATH, OAUTH2_REDIRECT_URI} from "./RestContants";


export const GOOGLE_AUTH_URL = BASE_PATH + 'oauth2/authorize/google?redirect_uri=' + OAUTH2_REDIRECT_URI;
export const FACEBOOK_AUTH_URL = BASE_PATH + 'oauth2/authorize/facebook?redirect_uri=' + OAUTH2_REDIRECT_URI;
export const GITHUB_AUTH_URL = BASE_PATH + 'oauth2/authorize/github?redirect_uri=' + OAUTH2_REDIRECT_URI;

