export const ACCESS_TOKEN = "AccessToken";
export const REFRESH_TOKEN = "RefreshToken";
export const USERNAME = "username";
export const BASE_PATH = 'http://localhost/';
export const OAUTH2_REDIRECT_URI = 'http://localhost:3000/ketmon'
export const OAUTH2_CONNECT_URI = 'http://localhost:3000/cabinet/account'
export const AUTH_SERVICE_V1 = "api/";