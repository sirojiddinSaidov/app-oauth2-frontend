import React, {useState} from 'react';
import {useNavigate} from "react-router-dom";
import '../pages/index.css'
import alert from "bootstrap/js/src/alert";

const LoginComponent = () => {
    const [phoneNumber, setPhoneNumber] = useState('+998');
    const [password, setPassword] = useState();

    const setPass = (e) => setPassword(e.target.value);
    const navigate = useNavigate();

    return (
        <>
            <div className="form-item">
                <input type="email"
                       name="email"
                       placeholder="Enter phone number"
                       onChange={(e) => setPhoneNumber(e.target.value)}
                       className="form-control"
                       value={phoneNumber}
                       required/>
            </div>
            <div className="form-item">
                <input
                    type="password"
                    name="password"
                    className="form-control" placeholder="Password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)} required/>
            </div>
            <div className="form-item">
                <button
                    onClick={() => alert(5)}
                    className="btn btn-block btn-primary">Login
                </button>
            </div>
        </>
    );
}

export default LoginComponent;